'use strict';

window.addEventListener("DOMContentLoaded", () => {
    const rooms = document.querySelectorAll(".room-wrapper");
    
    let getPrice = (allRooms) => {
        let roomNum;
        let allPrice = [];
        
        for(let i = 0; i < allRooms.length; i++) {
            roomNum = parseInt(allRooms[i].dataset.price.replace(",", "").split(".", 1));
            
            allPrice.push(roomNum);
        }
    
        return allPrice;
    
    };

    let sortPrice = (allPrice) => {
        
        return(
            allPrice.sort( (a, b) => {
                return a - b; 
            }
        ));
        
    };

    let sortRooms = (allPrice, allRooms) => {
        let roomPrice;
        let sortedRooms = [];
        
        for(let rp = 0; rp < allPrice.length; rp++) {
            
            for(let rpt = 0; rpt < allRooms.length; rpt++){
                
                roomPrice = parseInt(allRooms[rpt].dataset.price.replace(",", "").split(".", 1));
                
                if(allPrice[rp] === roomPrice) {
                    sortedRooms.push(allRooms[rpt]);
                }
            }
        }
        
        return sortedRooms;
        
    };

    let showRooms = (allRooms) => {
        let allPrice = getPrice(allRooms);
        let sortedPrice = sortPrice(allPrice);
        let sortedRooms = sortRooms(sortedPrice, allRooms);
        let roomsWrapper = document.querySelector("#jsSortItems");
        
        for(let sr = 0; sr < sortedRooms.length; sr++) {
            roomsWrapper.appendChild(sortedRooms[sr]);
        }
        
        return roomsWrapper;
        
    };
    
    showRooms(rooms);
});