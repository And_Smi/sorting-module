'use strict';

window.addEventListener("DOMContentLoaded", function () {
  var rooms = document.querySelectorAll(".room-wrapper");

  var getPrice = function getPrice(allRooms) {
    var roomNum;
    var allPrice = [];

    for (var i = 0; i < allRooms.length; i++) {
      roomNum = parseInt(allRooms[i].dataset.price.replace(",", "").split(".", 1));
      allPrice.push(roomNum);
    }

    return allPrice;
  };

  var sortPrice = function sortPrice(allPrice) {
    return allPrice.sort(function (a, b) {
      return a - b;
    });
  };

  var sortRooms = function sortRooms(allPrice, allRooms) {
    var roomPrice;
    var sortedRooms = [];

    for (var rp = 0; rp < allPrice.length; rp++) {
      for (var rpt = 0; rpt < allRooms.length; rpt++) {
        roomPrice = parseInt(allRooms[rpt].dataset.price.replace(",", "").split(".", 1));

        if (allPrice[rp] === roomPrice) {
          sortedRooms.push(allRooms[rpt]);
        }
      }
    }

    return sortedRooms;
  };

  var showRooms = function showRooms(allRooms) {
    var allPrice = getPrice(allRooms);
    var sortedPrice = sortPrice(allPrice);
    var sortedRooms = sortRooms(sortedPrice, allRooms);
    var roomsWrapper = document.querySelector("#jsSortItems");

    for (var sr = 0; sr < sortedRooms.length; sr++) {
      roomsWrapper.appendChild(sortedRooms[sr]);
    }

    return roomsWrapper;
  };

  showRooms(rooms);
});